import CategorySection from "@/Components/Site/CategorySection";
import NewProductsSection from "@/Components/Site/NewProductsSection";
import SlidingBanner from "@/Components/Site/SlidingBanner";
import TrendingProduct from "@/Components/Site/TrendingProducts";
import SiteLayout from "@/Layouts/SiteLayout";

function Index() {
  return (
    <SiteLayout>
      <CategorySection />
      <NewProductsSection />
      <SlidingBanner />
      <TrendingProduct />
    </SiteLayout>
  );
}

export default Index;
