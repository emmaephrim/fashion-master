import payment1 from "../../img/payment/payment-1.png";
import payment2 from "../../img/payment/payment-2.png";
import payment3 from "../../img/payment/payment-3.png";
import payment4 from "../../img/payment/payment-4.png";
import payment5 from "../../img/payment/payment-5.png";
import category1 from "../../img/categories/category-1.jpg";
import category2 from "../../img/categories/category-2.jpg";
import category3 from "../../img/categories/category-3.jpg";
import category4 from "../../img/categories/category-4.jpg";
import category5 from "../../img/categories/category-5.jpg";
import product1 from "../../img/product/product-1.jpg";
import product2 from "../../img/product/product-2.jpg";
import product3 from "../../img/product/product-3.jpg";
import product4 from "../../img/product/product-4.jpg";
import product5 from "../../img/product/product-5.jpg";
import product6 from "../../img/product/product-6.jpg";
import product7 from "../../img/product/product-7.jpg";
import product8 from "../../img/product/product-8.jpg";

export function importImages() {
  let images = [payment1, payment2, payment3, payment4, payment5];
  return images;
}

export const categoryImgs = {
  category1,
  category2,
  category3,
  category4,
  category5,
};

export function newProductItems() {
  let items = [
    {
      id: 0,
      imgUrl: product1,
      name: "Buttons tweed blazer",
      price: 59.0,
      rating: 1,
      tag: "NEW",
      status: "new",
      cat: "women",
    },
    {
      id: 1,
      imgUrl: product2,
      name: "Flowy striped skirt",
      price: 49.0,
      rating: 4,
      tag: "",
      status: "",
      cat: "kid",
    },
    {
      id: 2,
      imgUrl: product3,
      name: "Cotton T-Shirt",
      price: 79.0,
      rating: 5,
      tag: "OUT OF STOCK",
      status: "stockout",
      cat: "women",
    },
    {
      id: 3,
      imgUrl: product4,
      name: "Slim striped pocket shirt",
      price: 70.0,
      rating: 5,
      tag: "",
      sale: false,
      cat: "men",
    },
    {
      id: 4,
      imgUrl: product5,
      name: "Fit micro corduroy shirt",
      price: 32.0,
      rating: 2,
      tag: "",
      sale: false,
      cat: "kid",
    },
    {
      id: 5,
      imgUrl: product6,
      name: "Tropical Kimono",
      price: 40.0,
      rating: 3,
      tag: "Sale",
      status: "sale",
      cat: "women",
    },
    {
      id: 5,
      imgUrl: product7,
      name: "Contrasting sunglasses",
      price: 654.0,
      rating: 5,
      tag: "",
      cat: "women",
    },
    {
      id: 6,
      imgUrl: product8,
      name: "Water resistant backpack",
      price: 64.0,
      rating: 4,
      tag: "Sale",
      status: "sale",
      cat: "men",
    },
  ];
  return items;
}
