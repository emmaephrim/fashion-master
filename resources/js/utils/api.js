export async function importAllProducts() {
  try {
    const response = await fetch(
      "https://api.chec.io/v1/products?include=attributes,extra_fields,related_products,variant_groups&sortBy=created_at&sortDirection=asc",
      {
        headers: {
          "X-Authorization": import.meta.env.VITE_CHEC_API_LIVE_SK_KEY,
        },
      }
    );
    let products = await response.json();
    console.log("Products: ", products);
    return products;
  } catch (error) {
    console.log("API FETCH ERROR: ", error);
  }
}
