import Footer from "@/Components/Site/Footer";
import Header from "@/Components/Site/Header";
import "../../css/styles/bootstrap.min.css";
import "../../css/styles/elegant-icons.css";
import "../../css/styles/font-awesome.min.css";
import "../../css/styles/jquery-ui.min.css";
import "../../css/styles/magnific-popup.css";
import "../../css/styles/owl.carousel.min.css";
import "../../css/styles/slicknav.min.css";
import "../../css/styles/style.css";

// const scripts = import.meta.glob("../scripts/**/*.js");
// const styles = import.meta.glob("../../css/styles/**/*.css");

function SiteLayout({ children }) {
  return (
    <>
      <Header />
      {children}
      <Footer />
    </>
  );
}

export default SiteLayout;
