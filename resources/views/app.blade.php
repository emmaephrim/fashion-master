<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" type="image/x-icon">

    <title inertia>{{ config('app.name', 'Laravel') }}</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Cookie&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <!-- Scripts -->
    @routes
    @viteReactRefresh
    @vite(['resources/css/styles/bootstrap.min.css'])
    @vite(['resources/js/app.jsx', "resources/js/Pages/{$page['component']}.jsx"])
    @vite(['resources/js/scripts/jquery-3.3.1.min.js'])
    @vite(['resources/js/scripts/jquery.magnific-popup.min.js'])
    @vite(['resources/js/scripts/jquery-ui.min.js'])
    @vite(['resources/js/scripts/mixitup.min.js'])
    @vite(['resources/js/scripts/jquery.countdown.min.js'])
    @vite(['resources/js/scripts/jquery.slicknav.js'])
    @vite(['resources/js/scripts/owl.carousel.min.js'])
    @vite(['resources/js/scripts/jquery.nicescroll.min.js'])
    @vite(['resources/js/scripts/main.js'])
    @inertiaHead
</head>

<body class="font-sans antialiased">
    @inertia
</body>

</html>
